from journey_status     import *
from cities.models      import City
from filer.fields.image import FilerImageField

from django.db                  import models
from django.utils.timezone      import now
from django.core.exceptions     import ValidationError
from django.utils.translation   import ugettext as _
from django.contrib.auth.models import AbstractUser


class Customer(AbstractUser):
    city    = models.ForeignKey(City, related_name='+', verbose_name=_('city'))
    picture = FilerImageField(null=True, blank=True, verbose_name=_('picture'))

    class Meta:
        verbose_name        = _('customer')
        verbose_name_plural = _('customers')

    def participate_to_journey(self, journey):
        participation = Passengers(customer=self, journey=journey)
        participation.save()


class Journey(models.Model):
    driver         = models.ForeignKey(Customer, related_name='+', verbose_name=_('driver'))
    arrival_city   = models.ForeignKey(City, related_name='+', verbose_name=_('arrival city'))
    departure_city = models.ForeignKey(City, related_name='+', verbose_name=_('departure city'))

    arrival_date   = models.DateTimeField(verbose_name=_('arrival date'))
    departure_date = models.DateTimeField(verbose_name=_('departure date'))

    price          = models.DecimalField(max_digits=6, decimal_places=2, verbose_name=_('price'))
    status         = models.IntegerField(choices=JOURNEY_STATUS, verbose_name=_('status'))
    comments       = models.TextField(null=True, blank=True, verbose_name=_('comments'))
    places         = models.PositiveIntegerField(verbose_name=_('places'))
    passengers     = models.ManyToManyField(Customer, through='Passengers', verbose_name=_('passengers'))

    class Meta:
        verbose_name        = _('journey')
        verbose_name_plural = _('journeys')

    def __unicode__(self):
        return _('journey from {0} to {1} starting on {2} to {3} [{4}]').format(
            self.departure_city,
            self.arrival_city,
            self.departure_date,
            self.arrival_date,
            self.status
        )

    def clean(self):
        if self.departure_date >= self.arrival_date:
            raise ValidationError(_('arrival date should be latter than departure date'))
        if self.price < 0:
            raise ValidationError(_('price cannot not be a negative number'))
        if self.places < 1:
            raise ValidationError(_('it is called sharing for a raison, so where do I sit ??'))
        if self.departure_date <= now() or self.arrival_date <= now():
            raise ValidationError(_('you cannot use dates in the paste, if necessary use the timetraveling machine ;)'))
        if self.id is not None and (
            self.status == JOURNEY_STARTED  or
            self.status == JOURNEY_ENDED_OK or
            self.status == JOURNEY_ENDED_KO
        ) and self.passengers.customer.count() == 0:
            raise ValidationError(_('cannot switch to any of these statuses without passengers'))

    @property
    def available_places(self):
        return self.places - self.passengers.count()


class Passengers(models.Model):
    journey  = models.ForeignKey(Journey, related_name='journeys', verbose_name=_('journey'))
    customer = models.ForeignKey(Customer, related_name='customers', verbose_name=_('customer'))

    class Meta:
        verbose_name        = _('passengers')
        verbose_name_plural = _('passengers')
        unique_together     = ("journey", "customer")

    def __unicode__(self):
        return _('{0} participating to the journey {1}').format(
            self.customer,
            self.journey
        )

    def clean(self):
        if self.journey.status != JOURNEY_PENDING:
            raise ValidationError(_('can only add passenger to pending journey'))
        if self.customer == self.journey.driver:
            raise ValidationError(_('the drvier cannot be also be a passenger'))
        if self.journey.available_places <= 1:
            raise ValidationError(_("no more places in this trip"))
