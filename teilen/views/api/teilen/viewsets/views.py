from teilen.models import Journey
from teilen.models import Customer
from teilen.models import Passengers

from rest_framework.response    import Response
from rest_framework.viewsets    import ModelViewSet
from rest_framework.renderers   import JSONRenderer
from rest_framework.permissions import IsAuthenticated

from teilen.serializers.teilen.customer   import Serializer        as CustomerSerializer
from teilen.serializers.teilen.journey    import DetailsSerializer as JourneyDetailsSerializer
from teilen.serializers.teilen.customer   import DetailsSerializer as CustomerDetailsSerializer
from teilen.serializers.teilen.passengers import DetailsSerializer as PassengersDetailsSerializer


class Customers(ModelViewSet):
    queryset           = Customer.objects.all()
    serializer_class   = CustomerSerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def highlight(self, request, pk, *args, **kwargs):
        return Response({"extra": True})


class Profile(ModelViewSet):
    model              = Customer
    serializer_class   = CustomerDetailsSerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return Response(self.request.user)


class ProfileShared(ModelViewSet):
    model              = Journey
    serializer_class   = JourneyDetailsSerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Journey.objects.filter(driver=self.request.user)


class ProfileJourneys(ModelViewSet):
    model              = Passengers
    serializer_class   = PassengersDetailsSerializer
    renderer_classes   = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Passengers.objects.filter(customer=self.request.user)
