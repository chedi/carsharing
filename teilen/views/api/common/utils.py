MAX_AUTOCOMPLETE_ROWS = 20


def autocomplete_helper(request):
    rows    = request.REQUEST['rows']
    return (
        request.REQUEST['pattern'],
        rows if rows <= MAX_AUTOCOMPLETE_ROWS else MAX_AUTOCOMPLETE_ROWS)
