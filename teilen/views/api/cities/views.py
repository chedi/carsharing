from teilen.views.api.cities.viewsets.views     import *
from teilen.views.api.cities.autocomplete.views import *


CitiesList        = Cities   .as_view({'get': 'list'})
RegionsList       = Regions  .as_view({'get': 'list'})
CountriesList     = Countries.as_view({'get': 'list'})

CitiesRetrieve    = Cities   .as_view({'get': 'retrieve'})
RegionsRetrieve   = Regions  .as_view({'get': 'retrieve'})
CountriesRetrieve = Countries.as_view({'get': 'retrieve'})

CitiesDetail      = Cities   .as_view({'get': 'details'})
RegionsDetail     = Regions  .as_view({'get': 'details'})
CountriesDetail   = Countries.as_view({'get': 'details'})

CitiesAuto        = CitiesAutocomplete   .as_view()
RegionsAuto       = RegionsAutocomplete  .as_view()
CountriesAuto     = CountriesAutocomplete.as_view()
