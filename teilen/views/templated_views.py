from rest_framework.views     import APIView
from rest_framework.response  import Response
from rest_framework.renderers import TemplateHTMLRenderer


class ResetPassword(APIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        return Response({}, template_name='reset_passwd.html')


class Registration(APIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        return Response({}, template_name='registration.html')


class Index(APIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        return Response({}, template_name='index.html')
