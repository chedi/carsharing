import os

os.environ['HTTPS'          ] = "on"
os.environ['wsgi.url_scheme'] = 'https'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Mitfahrgelegenheit.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
