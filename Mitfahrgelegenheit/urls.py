import debug_toolbar
import autocomplete_light

from django.conf      import settings
from django.contrib   import admin
from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls import patterns

from social.apps.django_app  import urls as social_urls
from teilen.views.api.cities import urls as cities_urls
from teilen.views.api.teilen import urls as teilens_urls

from teilen.views.templated_views import Index
from teilen.views.templated_views import Registration
from teilen.views.templated_views import ResetPassword

from teilen.views.api_views import Share_api
from teilen.views.api_views import Search_api
from teilen.views.api_views import Reservation_api
from teilen.views.api_views import Registration_api
from teilen.views.api_views import PictureUpload_api
from teilen.views.api_views import ResetPassword_api


from django.contrib.staticfiles.urls import staticfiles_urlpatterns

autocomplete_light.autodiscover()
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/'       , include(admin.site.urls          )),
    url(r'^__debug__/'   , include(debug_toolbar.urls       )),
    url(r'^autocomplete/', include('autocomplete_light.urls')),

    url(r'^login$' , 'django.contrib.auth.views.login' , {'template_name': 'authentification/login.html'}, name="login" ),
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page'    : '/'         }, name="logout"),

    url(r'^register/$'      , Registration .as_view(), name="registration"  ),
    url(r'^reset_password/$', ResetPassword.as_view(), name="reset_password"),

    url(r'^$'                        , Index            .as_view(), name='index'                  ),
    url(r'^api/share/$'              , Share_api        .as_view(), name='share_api'              ),
    url(r'^api/search/$'             , Search_api       .as_view(), name='search_api'             ),
    url(r'^api/reservation/$'        , Reservation_api  .as_view(), name='reservation_api'        ),
    url(r'^api/registration/$'       , Registration_api .as_view(), name='registration_api'       ),
    url(r'^api/reset_password/$'     , ResetPassword_api.as_view(), name='reset_password_api'     ),
    url(r'^api/picture_upload/$'     , PictureUpload_api.as_view(), name='picture_upload_api'     ),

    url(r''              , include(social_urls , namespace='social')),
    url(r'^api/cities/'  , include(cities_urls , namespace='cities')),
    url(r'^api/teilens/' , include(teilens_urls, namespace='teilen')),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns = patterns(
        '',
        url(r'', include('django.contrib.staticfiles.urls')),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    ) + urlpatterns
