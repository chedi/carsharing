import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

DEBUG             = True
TEMPLATE_DEBUG    = DEBUG
PROJECT_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def getpath(resource_path):
    return os.path.abspath(os.path.join(PROJECT_DIRECTORY, resource_path))


ADMINS = (('chedi toueiti', 'chedi.toueiti@gmail.com'),)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE'  : 'django.contrib.gis.db.backends.postgis',
        'NAME'    : 'Mitfahrgelegenheit',
        'USER'    : 'postgres',
        'PASSWORD': '',
        'HOST'    : 'localhost',
        'PORT'    : '5432',
    }
}

ALLOWED_HOSTS = []
TIME_ZONE     = 'America/Chicago'
LANGUAGE_CODE = 'en-us'

SITE_ID  = 1
USE_I18N = True
USE_L10N = True
USE_TZ   = True

CSRF_COOKIE_SECURE    = True
SESSION_COOKIE_SECURE = True

MEDIA_ROOT  = getpath('media')
STATIC_ROOT = getpath('static')

MEDIA_URL  = '/media/'
STATIC_URL = '/static/'

AUTH_USER_MODEL     = 'teilen.Customer'
AUTH_PROFILE_MODULE = 'teilen.Customer'

STATICFILES_DIRS = (getpath('common_static'),)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

SECRET_KEY = '^ozsv-sp7x=h_yqxwm*^(n4%e)4ljig_@2obcri=h@#865g)!b'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
#    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
#    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF     = 'Mitfahrgelegenheit.urls'
WSGI_APPLICATION = 'Mitfahrgelegenheit.wsgi.application'
TEMPLATE_DIRS    = (getpath('templates'),)

'''
TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)'''

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.gis',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'suit',
    'filer',
    'south',
    'cities',
    'gunicorn',
    'compressor',
    'debug_toolbar',
    'rest_framework',
    'easy_thumbnails',
    'django_extensions',
    'autocomplete_light',
    'social.apps.django_app.default',

    'django.contrib.admin',

    'teilen',
)


TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


CITIES_FILES = {
    'city': {
        'filename': 'DE.zip',
        'urls'    : ['http://download.geonames.org/export/dump/'+'{filename}']
    },
}

CITIES_LOCALES = ['en', 'deu', 'LANGUAGES']
CITIES_PLUGINS = []
CITIES_POSTAL_CODES = ['DE', ]

SUIT_CONFIG = {
    'VERSION' : "0.1",

    'ADMIN_NAME'        : 'Mitfahrgelegenheit',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    'MENU_ICONS': {
        'sites': 'icon-leaf',
        'auth' : 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': True,

    'MENU': (
        {'app': 'teilen', 'label': 'Management'      },
        {'app': 'filer'     , 'label': "Files teilen"},
    ),

    'LIST_PER_PAGE': 30
}


LOCALE_PATHS  = (getpath('locale'),)
LANGUAGE_CODE = 'en'

gettext = lambda s: s

LANGUAGES = (
    ('en', gettext('English')),
    ('de', gettext('German' )),
)


FILER_STORAGES = {
    'public': {
        'main': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': getpath('media/public/filer'),
                'base_url': '/media/public/filer/',
            },
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PublicFileSystemStorage',
            'OPTIONS': {
                'location': getpath('media/public/filer_thumbnails'),
                'base_url': '/media/public/filer_thumbnails/',
            },
        },
    },
    'private': {
        'main': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': getpath('media/private/filer'),
                'base_url': '/smedia/filer/',
            },
            'UPLOAD_TO': 'filer.utils.generate_filename.by_date',
        },
        'thumbnails': {
            'ENGINE': 'filer.storage.PrivateFileSystemStorage',
            'OPTIONS': {
                'location': getpath('media/private/filer_thumbnails'),
                'base_url': '/smedia/filer_thumbnails/',
            },
        },
    },
}


DEFAULT_FILER_SERVERS = {
    'private': {
        'main': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        },
        'thumbnails': {
            'ENGINE': 'filer.server.backends.default.DefaultServer',
        }
    }
}

THUMBNAIL_ALIASES = {
    '': {
        's200' : {'size': (200 , 100 ), 'crop': True},
        's300' : {'size': (300 , 300 ), 'crop': True},
        's480' : {'size': (480 , 480 ), 'crop': True},
        's680' : {'size': (680 , 680 ), 'crop': True},
        's1000': {'size': (1000, 1000), 'crop': True},
    },
}

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
)

INTERNAL_IPS = ('127.0.0.1',)
DEBUG_TOOLBAR_PATCH_SETTINGS = False
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')


def show_toolbar(request):
    return False

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS'  : False,
    'ENABLE_STACKTRACES'   : True,
    'SHOW_TOOLBAR_CALLBACK': 'Mitfahrgelegenheit.settings.show_toolbar',
}


AUTHENTICATION_BACKENDS = (
    'social.backends.open_id.OpenIdAuth',
    'social.backends.google.GoogleOpenId',
    'social.backends.google.GoogleOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)
